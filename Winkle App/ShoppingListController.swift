//
//  ShoppingListTableViewController.swift
//  Winkle App
//
//  Created by Maurice van Breukelen on 18-03-15.
//  Copyright (c) 2015 Maurice en Merijn. All rights reserved.
//

import UIKit
import CoreLocation

class ShoppingListController: UITableViewController {
    
    var shoppingList : ShoppingList? {
        didSet {
            self.loadShoppingListInTableView()
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.tableFooterView = UIView(frame: CGRectZero)
        tableView.registerClass(UITableViewCell.classForCoder(), forCellReuseIdentifier: "itemCell")
        tableView.registerNib(UINib(nibName: "AddItemCell", bundle: nil), forCellReuseIdentifier: "AddItemCell")
    }
    
    override func viewWillAppear(animated: Bool) {
		self.navigationController?.navigationBar.translucent = false
		self.setSupermarketStyle()
		
        self.loadShoppingListInTableView()
    }
	
	override func viewWillDisappear(animated: Bool) {
		self.resetSupermarketStyle()
	}
    
    func getListCount() -> Int {
        if let items = self.shoppingList?.items {
            return self.shoppingList!.items.count
        } else {
            return 0
        }
    }
    
    func loadShoppingListInTableView(scrollToBottom : Bool = false) {
        self.tableView.reloadData()
        
        if let supermarket = self.shoppingList?.supermarket {
            self.title = supermarket.name
        }
        
        if scrollToBottom {
            var indexPath = NSIndexPath(forRow: self.getListCount(), inSection: 0)
            self.tableView.scrollToRowAtIndexPath(indexPath, atScrollPosition: UITableViewScrollPosition.Bottom, animated: true)
        }
    }
    
    func addItem(name: String) {
        var json = JSON(["name":name, "checked" : false])
        if let shoppinglist = self.shoppingList {
            HTTPController.PUT("shoppinglist/\(shoppinglist.id!)", httpBody: json.rawData()) { (json, error) -> Void in
                if error == nil {
                    var item = Item()
                    item.name = name
                    item.checked = false
					item.id = json?.rawString()!
					item.shoppingList = self.shoppingList
					
                    self.shoppingList?.items.append(item)
                    self.tableView.beginUpdates()
                    self.tableView.insertRowsAtIndexPaths(NSArray(object: NSIndexPath(forRow: self.getListCount()-1, inSection: 0)) as [AnyObject], withRowAnimation: UITableViewRowAnimation.Automatic)
                    self.tableView.endUpdates()
                    self.loadShoppingListInTableView(scrollToBottom: true)
                }
            }
        }
    }
	
	
	// MARK: - View changes
	
	func setSupermarketStyle() {
		if let supermarket = self.shoppingList?.supermarket {
			var style : UIStatusBarStyle = (supermarket.light) ? .LightContent : .Default;
			(self.navigationController as! NavigationControllerWithWhiteStatusBar).setColor(style)
			
			if let textcolour = supermarket.textcolour {
				self.navigationController?.navigationBar.titleTextAttributes = [NSForegroundColorAttributeName : UIColor(rgba: textcolour)]
				self.navigationController?.navigationBar.tintColor = UIColor(rgba: textcolour)
			}
			
			if let backgroundcolour = supermarket.backgroundcolour {
				self.navigationController?.navigationBar.barTintColor = UIColor(rgba: backgroundcolour)
			}
		}
	}
	
	func resetSupermarketStyle() {
		(self.navigationController as! NavigationControllerWithWhiteStatusBar).setColor(.Default)
		self.navigationController?.navigationBar.titleTextAttributes = nil
		self.navigationController?.navigationBar.tintColor = nil
		self.navigationController?.navigationBar.barTintColor = nil
	}
	
	func showAlert(title: String, message: String, buttonTitle: String) {
		let alertController = UIAlertController(title: title, message: message, preferredStyle: UIAlertControllerStyle.Alert)
		alertController.addAction(UIAlertAction(title: buttonTitle, style: UIAlertActionStyle.Default, handler: nil))
		self.presentViewController(alertController, animated: true, completion: nil)
	}
	
	
    // MARK: - Table view delegates and data source
    
    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if let shoppinglist = self.shoppingList {
            return self.getListCount() + 1
        } else {
            return 0
        }
    }
    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        var cell : UITableViewCell
        
        if(indexPath.row < self.getListCount())
        {
            cell = tableView.dequeueReusableCellWithIdentifier("itemCell", forIndexPath: indexPath) as! UITableViewCell
            var item = self.shoppingList!.items[indexPath.row] as Item
            cell.textLabel?.text = item.name!
            
            if item.checked {
                cell.accessoryType = UITableViewCellAccessoryType.Checkmark
            } else {
                cell.accessoryType = UITableViewCellAccessoryType.None
            }
        }
        else
        {
            cell = tableView.dequeueReusableCellWithIdentifier("AddItemCell", forIndexPath: indexPath) as! AddItemCell
            (cell as! AddItemCell).tableViewController = self
            
        }
        
        return cell
    }
    
    override func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return 44.0
    }
    
    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        var cell = tableView.cellForRowAtIndexPath(indexPath)! as UITableViewCell
        tableView.deselectRowAtIndexPath(indexPath, animated: true)
        
        if !(cell is AddItemCell) {
			
            var item = self.shoppingList!.items[indexPath.row] as Item
            item.toggleCheck()
            
            var itemJson = JSON(["name": item.name!, "checked": item.checked])
            HTTPController.PUT("shoppinglist/\(item.shoppingList!.id!)/item/\(item.id!)", httpBody: itemJson.rawData(), callback: { (json, error) -> Void in
                if error == nil {
                    tableView.reloadData()
                }
            })
        }
    }
}
