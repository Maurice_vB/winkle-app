//
//  HTTPController.swift
//  Winkle App
//
//  Created by Maurice van Breukelen on 16-03-15.
//  Copyright (c) 2015 Maurice en Merijn. All rights reserved.
//

import Foundation

class HTTPController: NSObject {
    
    class var debug: Bool {
        return false
    }
    
    /**
    All possible HTTP-methods (REST)
    
    - GET:      For getting an object from the server
    - POST:     For creating a new object on the server
    - PUT:      For updating an object on the server
    - DELETE:   For deleting an object on the server
    */
    enum HTTP {
        case GET, POST, PUT, DELETE
        
        var stringValue : String {
            switch self {
                case .GET:      return "GET";
                case .POST:     return "POST";
                case .PUT:      return "PUT";
                case .DELETE:   return "DELETE";
            }
        }
    }
    
	//private class var serverUrl: String {
	//	return "http://84.27.0.138:3000"
	//}
	
    /**
    Server URL used as a prefix for request URL's
    */
	private class var serverUrl: String {
		return "http://winkle.herokuapp.com"
	}
	
    
    // MARK: - REST methods with data
	
	class func GET(urlString: String!, httpBody: NSData?, callback: (JSON?, String?) -> Void) {
		httpRequest(.GET, urlString: urlString, httpBody: httpBody) { (json, error) -> Void in
			callback(json, error)
		}
	}
	
	class func POST(urlString: String!, httpBody: NSData?, callback: (JSON?, String?) -> Void) {
		httpRequest(.POST, urlString: urlString, httpBody: httpBody) { (json, error) -> Void in
			callback(json, error)
		}
	}
	
    class func PUT(urlString: String!, httpBody: NSData?, callback: (JSON?, String?) -> Void) {
        httpRequest(.PUT, urlString: urlString, httpBody: httpBody) { (json, error) -> Void in
            callback(json, error)
        }
    }
    
	class func DELETE(urlString: String!, httpBody: NSData?, callback: (JSON?, String?) -> Void) {
		httpRequest(.DELETE, urlString: urlString, httpBody: httpBody) { (json, error) -> Void in
			callback(json, error)
		}
	}
	
	
	// MARK: - REST methods without data
	
	class func GET(urlString: String!, callback: (JSON?, String?) -> Void) {
        httpRequest(.GET, urlString: urlString, httpBody: nil) { (json, error) -> Void in
			callback(json, error)
		}
	}
	
	class func POST(urlString: String!, callback: (JSON?, String?) -> Void) {
		httpRequest(.POST, urlString: urlString, httpBody: nil) { (json, error) -> Void in
			callback(json, error)
		}
	}
	
	class func PUT(urlString: String!, callback: (JSON?, String?) -> Void) {
		httpRequest(.PUT, urlString: urlString, httpBody: nil) { (json, error) -> Void in
			callback(json, error)
		}
	}
	
	class func DELETE(urlString: String!, callback: (JSON?, String?) -> Void) {
		httpRequest(.DELETE, urlString: urlString, httpBody: nil) { (json, error) -> Void in
			callback(json, error)
		}
	}
	
	
    // MARK: - Request method
    
    /**
    Creates a request object with the given URL string and executes it.
    
    :param: method      The HTTP-method (enum HTTP: GET, POST, PUT, DELETE)
    :param: urlString   The URL to be called. An full URL or just the part after the hostname
    :param: httpBody    The data to be sent. Mostly an JSON-object converted to a NSData object
    :param: callback    A block, returns an JSON-object and en error-string (if an error occured)
    
    :returns:
    */
    private class func httpRequest(method: HTTP, urlString: String!, httpBody: NSData?, callback: (JSON?, String?) -> Void) {
        // Create full request URL
        var urlString = urlString;
		
		// Add slash before URI string
		var firstChar = Array(urlString)[0]
		if firstChar != "/" {
			urlString = "/" + urlString
		}
		
		// Create HTTP-URL when only a path is given
        if !NSString(string: urlString).containsString("http://") {
            urlString = serverUrl + urlString
        }
		
        // Create request
        var request = NSMutableURLRequest(URL: NSURL(string: urlString)!)
        request.HTTPMethod = method.stringValue;
        request.setValue("application/json", forHTTPHeaderField: "Accept")
        
        // Set HTTP body (if provided)
        if httpBody != nil {
            request.HTTPBody = httpBody!
            request.setValue("application/json", forHTTPHeaderField: "Content-Type")
            request.setValue(String(httpBody!.length), forHTTPHeaderField: "Content-Length")
        }
        
        // Set Authorization-header when the user is logged in
        if User.sharedInstance.isLoggedIn() {
            request.setValue(User.sharedInstance.token, forHTTPHeaderField: "Authorization")
        }
        
        // Create a NSURLSession and handle response
        var session = NSURLSession.sharedSession()
        var task = session.dataTaskWithRequest(request) { (data, response, error) -> Void in
			
			if let httpResponse = response as? NSHTTPURLResponse  {
				dispatch_async(dispatch_get_main_queue()) {
					// An error occurred
					if error != nil {
						callback(nil, error.localizedDescription)
						
					// An error status code returned (all values greater than 399)
					} else if httpResponse.statusCode >= 400 {
						callback(nil, "Error: status code \(httpResponse.statusCode)")
		
					// No errors occurred and no error status code has been returned
					} else {
						var dataString = NSString(data: data!, encoding: NSUTF8StringEncoding)
						
						// When no empty JSON returned
						if dataString != "{}" {
							let json = JSON(data: data)
							callback(json, nil)
							
						// Empty JSON returned
						} else {
							callback(nil, "Error: empty JSON string returned from server")
						}
					}
					
					// Print response in debug mode
					if self.debug {
						println(urlString)
						println(NSString(data:data, encoding:NSUTF8StringEncoding)!)
					}
				}
			}
        }
		
		// Execute request task created above
        task.resume()
    }
}