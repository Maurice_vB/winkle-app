//
//  NavigationControllerWithWhiteStatusBar.swift
//  Winkle App
//
//  Created by Maurice van Breukelen on 20-03-15.
//  Copyright (c) 2015 Maurice en Merijn. All rights reserved.
//

import UIKit

class NavigationControllerWithWhiteStatusBar: UINavigationController {
    
    var statusBarStyle : UIStatusBarStyle = .Default
    
    func setColor(statusBarStyle : UIStatusBarStyle) {
        self.statusBarStyle = statusBarStyle
        self.setNeedsStatusBarAppearanceUpdate()
    }
    
    override func preferredStatusBarStyle() -> UIStatusBarStyle {
        return self.statusBarStyle
    }

}
