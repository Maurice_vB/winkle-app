//
//  Item.swift
//  Winkle App
//
//  Created by Merijn Celie on 19-03-15.
//  Copyright (c) 2015 Maurice en Merijn. All rights reserved.
//

import UIKit

class Item: NSObject {
    var id : String?
	var name : String?
	var checked : Bool = false
    var shoppingList : ShoppingList?
	
    // Create and return Item object from JSON object
	class func itemFromJsonObject(json : JSON) -> Item {
		var item = Item()
        item.id = json["_id"].stringValue
		item.name = json["name"].stringValue
		item.checked = json["checked"].boolValue
		
		return item
	}
	
    // Toggle "checked" value
    func toggleCheck() {
        self.checked = !self.checked
    }
}
