//
//  ShoppingListTableViewController.swift
//  Winkle App
//
//  Created by Maurice van Breukelen on 18-03-15.
//  Copyright (c) 2015 Maurice en Merijn. All rights reserved.
//

import UIKit
import CoreLocation

class CurrentShoppingListController: ShoppingListController, CLLocationManagerDelegate {
	
	var locationManager : CLLocationManager!
	var coordinates : CLLocationCoordinate2D? {
		didSet {
			self.loadClosestShoppingList(false)
		}
	}
	
    override func viewDidLoad() {
        super.viewDidLoad()
		self.locationManager = CLLocationManager()
    }
    
	override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
		
		self.locationManager.requestWhenInUseAuthorization()
		if CLLocationManager.locationServicesEnabled() {
			self.locationManager.delegate = self
			self.locationManager.desiredAccuracy = kCLLocationAccuracyHundredMeters
			self.locationManager.distanceFilter = 100
			self.locationManager.startUpdatingLocation()
		}
	}
	
    func loadClosestShoppingList(scrollToBottom : Bool) {
		if let currentCoordinates = self.coordinates {
			HTTPController.GET("supermarkets/current?lat=\(currentCoordinates.latitude)&lon=\(currentCoordinates.longitude)", callback: { (json, error) -> Void in
				if error == nil {
					self.shoppingList = ShoppingList.shoppingListFromJsonObject(json!)
                    self.loadShoppingListInTableView(scrollToBottom: scrollToBottom);
					self.setSupermarketStyle()
				}
			})
		}
	}
	
	// MARK: - Location manager delegate
	func locationManager(manager: CLLocationManager!, didUpdateLocations locations: [AnyObject]!) {
		self.coordinates = manager.location.coordinate
	}
}
