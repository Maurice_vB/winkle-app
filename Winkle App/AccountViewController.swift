//
//  AccountViewController.swift
//  Winkle App
//
//  Created by Merijn Celie on 16-03-15.
//  Copyright (c) 2015 Maurice en Merijn. All rights reserved.
//

import UIKit

class AccountViewController: UIViewController, UITextFieldDelegate {

    @IBOutlet weak var textfield_username: UITextField!
    @IBOutlet weak var textfield_password: UITextField!
    @IBOutlet weak var button_login: UIButton!
    @IBOutlet weak var button_register: UIButton!
    @IBOutlet weak var button_logout: UIButton!
    
    override func viewDidLoad() {
        if(User.sharedInstance.isLoggedIn()) {
			hideLoginScreen()
        } else {
			showLoginScreen()
        }
        
        textfield_username.delegate = self
        textfield_password.delegate = self
    }
    
    // MARK: - IBActions
    @IBAction func actionLogin(sender: AnyObject) {
        var username = textfield_username.text;
        var password = textfield_password.text;
        
        if username != "" && password != "" {
			var json = JSON(["username":username, "password":password])
            HTTPController.POST("/login", httpBody: json.rawData()) { (json, error) -> Void in
                if json != nil && error == nil {
                    if let token = json?["token"].string {
                        self.login(token)
                    }
                }
            }
        }
    }
    
    @IBAction func actionRegister(sender: AnyObject) {
        var username = textfield_username.text;
        var password = textfield_password.text;
        
        if username != "" && password != "" {
            var json = JSON(["username":username, "password":password])
            HTTPController.POST("/register", httpBody: json.rawData()) { (json, error) -> Void in
                if json != nil && error == nil {
                    if let token = json?["token"].string {
                        self.login(token)
                    }
                }
            }
        }
    }
    
    @IBAction func actionLogout(sender: AnyObject) {
        User.sharedInstance.logout()
        showLoginScreen()
    }
    

    // MARK: - Delegate methods
    
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        
        if textField.tag == 101 {
            // Username
            self.view.viewWithTag(102)?.becomeFirstResponder()
        }
        
        if textField.tag == 102 {
            // Password
            self.actionLogin(self)
        }
        
        return true;
    }
    
    
    // MARK: - View methods
    
    func login(token: String) {
        User.sharedInstance.login(token)
        self.hideLoginScreen()
    }
    
    func showLoginScreen() {
        textfield_username?.hidden = false
        textfield_password?.hidden = false
        button_logout?.hidden = true
        button_login?.hidden = false
        button_register?.hidden = false
    }
    
    func hideLoginScreen() {
        textfield_username?.hidden = true
        textfield_password?.hidden = true
        button_logout?.hidden = false
        button_login?.hidden = true
        button_register?.hidden = true
    }
}