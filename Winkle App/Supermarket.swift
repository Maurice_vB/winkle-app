//
//  Shop.swift
//  Winkle App
//
//  Created by Merijn Celie on 06-03-15.
//  Copyright (c) 2015 Maurice en Merijn. All rights reserved.
//

import Foundation

class Supermarket: NSObject {
	var id : String?
	var name : String?
	var synonyms : [String]?
	
	var backgroundcolour : String?
	var textcolour : String?
	var light : Bool = false
	
	
    // Create and return Supermarket object from JSON object
	class func supermarketFromJsonObject(json : JSON) -> Supermarket {
	
		var supermarket = Supermarket()
		supermarket.id = json["_id"].stringValue
		supermarket.name = json["name"].stringValue
		
		// Generate array of synonyms
		var synonyms = [String]()
		for (key: String, synonymJson: JSON) in json["synonyms"] {
			synonyms.append(synonymJson.stringValue)
		}
		supermarket.synonyms = synonyms
		
		// Set colours
		if let colours = json["colour"].dictionary {
			supermarket.backgroundcolour = colours["backgroundcolour"]?.stringValue
			supermarket.textcolour = colours["textcolour"]?.stringValue
			supermarket.light = colours["light"]!.boolValue
		}
		
		return supermarket
	}
}
