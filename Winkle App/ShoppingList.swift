//
//  ShoppingList.swift
//  Winkle App
//
//  Created by Merijn Celie on 18-03-15.
//  Copyright (c) 2015 Maurice en Merijn. All rights reserved.
//

import UIKit

class ShoppingList: NSObject {
	
	var id : String?
	var username : String?
	var supermarket : Supermarket?
	var items = [Item]()
	
	func remove() {
		HTTPController.DELETE("shoppinglist/\(self.id!)", callback: { (json, error) -> Void in
			if error == nil {
				self.id = nil
				self.username = nil
				self.supermarket = nil
			}
		})
	}
	
    // Create and return ShoppingList object from JSON object
	class func shoppingListFromJsonObject(json : JSON) -> ShoppingList {
		var shoppinglist = ShoppingList()
		shoppinglist.id = json["_id"].stringValue
		shoppinglist.username = json["username"].stringValue
		shoppinglist.supermarket = Supermarket.supermarketFromJsonObject(json["supermarket"])
		
		var items = [Item]()
		for (key: String, itemJson: JSON) in json["items"] {
            var item = Item.itemFromJsonObject(itemJson)
            item.shoppingList = shoppinglist
			items.append(item)
		}
		
		shoppinglist.items = items
	
		return shoppinglist
	}
}
