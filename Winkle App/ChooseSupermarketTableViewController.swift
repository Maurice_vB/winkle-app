//
//  ChooseSupermarketTableViewController.swift
//  Winkle App
//
//  Created by Merijn Celie on 18-03-15.
//  Copyright (c) 2015 Maurice en Merijn. All rights reserved.
//

import UIKit

class ChooseSupermarketTableViewController: UITableViewController {

	var supermarketList : NSMutableArray = []
	
    override func viewDidLoad() {
        super.viewDidLoad()
		self.title = "Alle winkels"
		self.tableView.registerNib(UINib(nibName: "SupermarketCell", bundle: nil), forCellReuseIdentifier: "SupermarketCell")
        loadSupermarkets()
    }
	
	func loadSupermarkets() {
		HTTPController.GET("supermarkets", callback: { (json, error) -> Void in
			if error == nil {
				self.supermarketList.removeAllObjects()
				for (key: String, subJson: JSON) in json! {
					self.supermarketList.addObject(Supermarket.supermarketFromJsonObject(subJson))
				}
				
				self.tableView.reloadData()
			}
		})
	}

    // MARK: - Table view data source

    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }

    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return supermarketList.count
    }

    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("SupermarketCell", forIndexPath: indexPath) as! SupermarketCell
		cell.accessoryType = UITableViewCellAccessoryType.DisclosureIndicator
		
		var supermarket : Supermarket = self.supermarketList.objectAtIndex(indexPath.row) as! Supermarket
		cell.logoImageView.image = UIImage(named: supermarket.name!)
		cell.nameLabel.text = supermarket.name!
		
        return cell
    }
	
	override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
		var supermarket = self.supermarketList.objectAtIndex(indexPath.row) as! Supermarket
		var viewcontroller = self.navigationController?.viewControllers.first as! AllShoppingListsController
		viewcontroller.supermarketToAdd = supermarket
		self.navigationController?.popToViewController(viewcontroller, animated: true)
	}
	
	override func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
		return 44.0
	}
}
