//
//  ManageShopsTableViewController.swift
//  Winkle App
//
//  Created by Maurice van Breukelen on 06/03/15.
//  Copyright (c) 2015 Maurice en Merijn. All rights reserved.
//

import UIKit

class AllShoppingListsController: UITableViewController {
    
    var inEditMode : Bool = false
	var shoppingLists : NSMutableArray = []
	var supermarketToAdd : Supermarket? {
		didSet(shop) {
			self.addShoppingList(supermarketToAdd!)
		}
	}
	
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationItem.rightBarButtonItem = self.editButtonItem()
		
		tableView.allowsSelectionDuringEditing = true
		tableView.registerNib(UINib(nibName: "SupermarketCell", bundle: nil), forCellReuseIdentifier: "SupermarketCell")
	}
	
	func loadShoppingLists() {
		HTTPController.GET("shoppinglist", callback: { (json, error) -> Void in
			if error == nil {
				self.shoppingLists.removeAllObjects()
				
				for (key: String, subJson: JSON) in json! {
					self.shoppingLists.addObject(ShoppingList.shoppingListFromJsonObject(subJson))
				}
				
				self.tableView.reloadData()
			}
		})
	}
	
	private func addShoppingList(supermarket : Supermarket) {
		var json = JSON(["supermarket":supermarket.id!, "items": [String]()])
		HTTPController.POST("shoppinglist", httpBody: json.rawData()) { (json, error) -> Void in
			if error == nil {
				self.loadShoppingLists()
			}
		}
		
	}
	
	override func viewDidAppear(animated: Bool) {
		self.loadShoppingLists()
	}
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

	
	// MARK: - Table view data source
	
    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
		return 1
    }

    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
		return (self.editing) ? shoppingLists.count + 1 : shoppingLists.count
    }

    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
		var cell : UITableViewCell
		
		if(indexPath.row < shoppingLists.count)
		{
			cell = tableView.dequeueReusableCellWithIdentifier("SupermarketCell", forIndexPath: indexPath) as! SupermarketCell
			var shoppinglist = shoppingLists.objectAtIndex(indexPath.row) as! ShoppingList			
			(cell as! SupermarketCell).logoImageView.image = UIImage(named: shoppinglist.supermarket!.name!)
			(cell as! SupermarketCell).nameLabel.text = shoppinglist.supermarket!.name!
		}
		else
		{
			cell = tableView.dequeueReusableCellWithIdentifier("addShopCell", forIndexPath: indexPath) as! UITableViewCell
		}
		
        return cell
    }
    
    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        if(indexPath.row < shoppingLists.count)
        {
            self.performSegueWithIdentifier("showShoppingList", sender: self.shoppingLists.objectAtIndex(indexPath.row))
        }
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if segue.identifier == "showShoppingList" {
            var vc = segue.destinationViewController as! ShoppingListController
            vc.shoppingList = sender as? ShoppingList
        }
    }

	override func tableView(tableView: UITableView, canMoveRowAtIndexPath indexPath: NSIndexPath) -> Bool {
		return true
	}
	
	override func tableView(tableView: UITableView, editingStyleForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCellEditingStyle {
        if(tableView.editing) {
            if indexPath.row == shoppingLists.count {
                return UITableViewCellEditingStyle.Insert
            } else {
                return UITableViewCellEditingStyle.Delete
            }
        } else {
            return UITableViewCellEditingStyle.None
        }

	}
    
    override func tableView(tableView: UITableView, willSelectRowAtIndexPath indexPath: NSIndexPath) -> NSIndexPath? {
        if indexPath.row < shoppingLists.count && tableView.editing
        {
            return nil
        }
        else
        {
            return indexPath
        }
    }
	
	override func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
		return 44.0
	}
	
	override func tableView(tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
		return "Boodschappenlijsten"
	}

	
	override func setEditing(editing: Bool, animated: Bool) {
		super.setEditing(editing, animated: animated)
		self.tableView.setEditing(editing, animated: animated)
		
		if(editing)
		{
			self.tableView.beginUpdates()
			self.tableView.insertRowsAtIndexPaths(NSArray(object: NSIndexPath(forRow: shoppingLists.count, inSection: 0)) as [AnyObject], withRowAnimation: UITableViewRowAnimation.Automatic)
			self.tableView.endUpdates()
		}
		else
		{
			self.tableView.beginUpdates()
			self.tableView.deleteRowsAtIndexPaths(NSArray(object: NSIndexPath(forRow: shoppingLists.count, inSection: 0)) as [AnyObject], withRowAnimation: UITableViewRowAnimation.Automatic)
			self.tableView.endUpdates()
		}
	}
	
    // Override to support editing the table view.
    override func tableView(tableView: UITableView, commitEditingStyle editingStyle: UITableViewCellEditingStyle, forRowAtIndexPath indexPath: NSIndexPath) {
        if editingStyle == .Delete {
            // Delete shopplinglist from both server and data source
			(self.shoppingLists.objectAtIndex(indexPath.row) as! ShoppingList).remove()
			self.shoppingLists.removeObjectAtIndex(indexPath.row)
			
			tableView.deleteRowsAtIndexPaths([indexPath], withRowAnimation: .Fade)
        } else if editingStyle == .Insert {
			performSegueWithIdentifier("showAllSupermarkts", sender: nil)
        }
    }

}
