//
//  User.swift
//  Winkle App
//
//  Created by Maurice van Breukelen on 16-03-15.
//  Copyright (c) 2015 Maurice en Merijn. All rights reserved.
//

import Foundation

class User: NSObject {
    
    var token : String?

    class var sharedInstance: User {
        struct Static {
            static let instance: User = User()
        }
        return Static.instance
    }
    
    override init() {
        var ud = NSUserDefaults.standardUserDefaults()
        self.token = ud.objectForKey("token") as? String
    }
    
    func login(token: String) {
        self.token = token
        var ud = NSUserDefaults.standardUserDefaults()
        ud.setObject(token, forKey: "token")
    }
    
    func logout() {
        self.token = nil;
        var ud = NSUserDefaults.standardUserDefaults()
        ud.removeObjectForKey("token")
        ud.synchronize()
    }
    
    func isLoggedIn() -> Bool {
        return token != nil
    }
}
