//
//  AddItemCell.swift
//  Winkle App
//
//  Created by Maurice van Breukelen on 19-03-15.
//  Copyright (c) 2015 Maurice en Merijn. All rights reserved.
//

import UIKit

class AddItemCell: UITableViewCell, UITextFieldDelegate {

	var tableViewController : ShoppingListController?
	@IBOutlet weak var itemTextField: UITextField!
	
    override func awakeFromNib() {
        super.awakeFromNib()
		self.itemTextField.delegate = self
    }
	
	// Add item when user hits the return button
	func textFieldShouldReturn(textField: UITextField) -> Bool {
		
		if let tableViewController = self.tableViewController {
			tableViewController.addItem(textField.text)
			textField.text = ""
		}
		
		textField.resignFirstResponder()
		
		return true
	}
    
}
